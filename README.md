# PathgraphEncoding

PathgraphEnconding is a library to encode and decode Pathgraph keys (public and private) to [ASN.1 DER format](http://luca.ntop.org/Teaching/Appunti/asn1.html "A Layman's Guide to a Subset of ASN.1, BER, and DER").

Pathgraph ANS.1 descriptor file is `PathGraphs.asn`.

## Installation

### Prerequisites

This a Ruby library packed into a [gem](http://guides.rubygems.org/what-is-a-gem/ "RubyGems Guides"), so you need Ruby interpreter installed, it is strongly recommended to use at least [Ruby 2.1.0](http://ruby-doc.org/core-2.1.0/ "Ruby 2.1.0 core documentation").

### Library installation

Add this line to your application's [Gemfile](http://bundler.io/v1.5/gemfile.html "Bundler documentation"):

```ruby
gem 'pathgraph_encoding'
```

then execute in shell:

```bash
$ bundle
```

Or install it yourself as:

```bash
$ gem install pathgraph_encoding
```

### Testing

This library has included a little suite of test cases to verify library algorithmic correctness. So it is suggested to execute tests before install or use this gem.

#### Executing tests

You can execute test cases with `rake` command:

```bash
$ rake test
```

## Library usage

This library has two specific tasks: encoding and decoding keys.

### Include library

You must include this library in your source code file, adding at the top a line like this:

```ruby
require 'pathgraph_encoding'
```

### Encoding a private key

Given an instance of a private key, like this:

```ruby
# Your private key
key = {
  n: 4,
  pi: [
    [0,1,3,2],
    [6,7,5,4],
    [12,13,15,14],
    [10,11,9,8]
  ]
}
```

call `PrivateKey::to_der` method:

```ruby
der = PathgraphEncoding::PrivateKey::to_der(key)
```

### Decoding a private key

Given an instance of a DER encoded key:

```ruby
der = "0F\x13\x050.0.10=\x02\x01\x04080\f\x02\x01\x00"\
      "\x02\x01\x01\x02\x01\x03\x02\x01\x020\f\x02\x01"\
      "\x06\x02\x01\a\x02\x01\x05\x02\x01\x040\f\x02\x01"\
      "\f\x02\x01\r\x02\x01\x0F\x02\x01\x0E0\f\x02\x01\n"\
      "\x02\x01\v\x02\x01\t\x02\x01\b"
```

call `PrivateKey::from_der` method:

```ruby
key = PathgraphEncoding::PrivateKey::from_der(der)
```

it will return a key like:

```ruby
key = {
  version: "0.0.2",
  n: 4,
  pi: [[0,1,3,2],[6,7,5,4],[12,13,15,14],[10,11,9,8]]
}
```


## Contributing

I will be grateful to have your contributions.
Feel free to make modifications, fix bugs or add new features.

1. Fork the project (https://gitlab.com/israelbuitron/pathgraph_encoding)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
