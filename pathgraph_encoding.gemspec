lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pathgraph_encoding/version'

Gem::Specification.new do |spec|
  spec.name          = "pathgraph_encoding"
  spec.version       = PathgraphEncoding::VERSION
  spec.authors       = ["Israel Buitron"]
  spec.email         = ["ibuitron@ipn.mx"]
  spec.summary       = %q{PathgraphEnconding is a library to encode Pathgraph keys to ASN.1}
  spec.description   = <<-EOF
    PathgraphEnconding is a library to encode and decode Pathgraph
    keys (public and private) to ASN.1 DER format.
  EOF
  spec.homepage      = "http://computacion.cs.cinvestav.mx/~ibuitron/"
  spec.license       = "MIT"
  spec.metadata      = { 'issue_tracker' => "https://gitlab.com/israelbuitron/pathgraph_encoding/issues" }

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake", '~> 10.4'
end
