require 'pathgraph_encoding/version'
require 'openssl'

# @author Israel Buitron
module PathgraphEncoding

  def self.pack(k, m, set)
    OpenSSL::ASN1::Sequence.new([
      OpenSSL::ASN1::Integer.new(k),
      OpenSSL::ASN1::Integer.new(m),
      OpenSSL::ASN1::Sequence.new(
        set.map { |path| 
          OpenSSL::ASN1::Sequence.new(
            path.map { |x| OpenSSL::ASN1::Integer.new(x) }
          )
        }
      )
    ]).to_der
  end

  def self.unpack(der)
    asn1 = OpenSSL::ASN1.decode(der)
    [
      OpenSSL::ASN1.decode(asn1.value[0]).value.to_i, # k
      OpenSSL::ASN1.decode(asn1.value[1]).value.to_i, # m
      OpenSSL::ASN1.decode(asn1.value[2]).value.map { |e| e.map { |d| d.value.to_i } }
    ]
  end

  # Represents a pathgraph private key.
  #
  # An instance of a private key can be like this:
  #
  #   key = {
  #     n: 4,
  #     pi: [[0,1,3,2],[6,7,5,4],[12,13,15,14],[10,11,9,8]]
  #   }
  module PrivateKey

    # Compute DER-formatted bytes array of a pathgraph private key.
    #
    # == Parameters:
    # key::
    #   A private key to encode.
    #
    # == Returns:
    # A byte array representing the private key specified.
    #
    def self.to_der(key)
      n = OpenSSL::ASN1::Integer.new(key[:n])
      pi = OpenSSL::ASN1::Sequence.new(
        key[:pi].map do |p|
          arr = p.map { |x| OpenSSL::ASN1::Integer.new(x) }
          OpenSSL::ASN1::Sequence.new(arr)
        end
      )
      privateKey = OpenSSL::ASN1::Sequence.new([n,pi])

      version = OpenSSL::ASN1::PrintableString.new("0.0.1")
      instance = OpenSSL::ASN1::Sequence.new([version,privateKey])

      instance.to_der
    end

    # Decode and build a instance of a pathgraph private key from a
    # DER-formatted bytes array.
    #
    # == Parameters:
    # der::
    #   A byte array representing the private key specified.
    #
    # == Returns:
    # key::
    #   The private key dencoded.
    #
    def self.from_der(der)
      asn1 = OpenSSL::ASN1.decode(der)
      {
        version: OpenSSL::ASN1.decode(asn1.value[0]).value,
        n: OpenSSL::ASN1.decode(asn1.value[1].value[0]).value.to_i,
        pi: OpenSSL::ASN1.decode(asn1.value[1].value[1]).value.map { |e| e.map { |d| d.value.to_i } }
      }
    end
    
    # Check if is a valid private key.
    #
    # == Parameters:
    # key::
    #   A byte array representing the private key specified.
    #
    # == Returns:
    # Returns `true` if is a valid private key, otherwise
    # `false`.
    #
    def self.is_valid?(key)
      # Check valid hypercube degree
      return false if is_valid_hypercube_degree(key[:n])

      # Check valid hypercube vertices
      # max_vertex = (1<<key[:n])-1
      key[:pi].each do |pi|
        pi.each do |vertex|
          return false unless is_valid_hypercube_vertex?(vertex,key[:n])
        end

        pi[0...-1].each_with_index do |i,j|
          return false unless is_adyacent_in_hp?(i,pi[j+1])
        end
      end

      true
    end

    # Check if a number is a valid hypercube degree is an integer
    # between 4 and 8.
    #
    # == Parameters:
    # n::
    #   Hypercube degree.
    #
    # == Returns:
    # If is a valid hypercube degree, returns `true`, otherwise
    # `false`.
    #
    def self.is_valid_hypercube_degree?(n)
      n >= 4 && n <= 8 && n.is_a?(Integer)
    end

    # Check if a number is a valid hypercube vertex.
    #
    # == Parameters:
    # v::
    #   Hypercube vertex.
    # n::
    #   Hypercube degree.
    #
    # == Returns:
    # If is a valid hypercube vertex, returns `true`, otherwise
    # `false`.
    #
    def self.is_valid_hypercube_vertex?(v,n)
      v >= 0 && v <= (1<<n)-1 && v.is_a?(Integer)
    end

    # Check if two vertices are adjacent in a hypercube.
    #
    # == Parameters:
    # a::
    #   First hypercube vertex.
    # b::
    #   Second hypercube vertex.
    #
    # == Returns:
    # If both are adjacent vertices, returns `true`, otherwise
    # `false`.
    #
    def self.is_adyacent_in_hp?(a,b)
      (a^b).to_s(2).count(1) == 1
    end

    # Generates a public key from a private key.
    #
    # == Parameters:
    # key::
    #   Public key.
    #
    # == Returns:
    # Public key related with private one parameterized.
    #
    def self.gen_public_key(key)
      sigma = []
      key[:pi].each { |p| sigma << [p.first,p.last] }
      {n: key[:n], sigma: sigma}
    end

  end

  # Represents a pathgraph public key.
  module PublicKey

    # Compute DER-formatted bytes array of a pathgraph public key.
    #
    # == Parameters:
    # key::
    #   A public key to encode.
    #
    # == Returns:
    # A byte array representing the public key specified.
    #
    def self.to_der(key)
      n = OpenSSL::ASN1::Integer.new(key[:n])
      sigma = OpenSSL::ASN1::Sequence.new(
        key[:sigma].map do |sig|
          arr = sig.map { |x| OpenSSL::ASN1::Integer.new(x) }
          OpenSSL::ASN1::Sequence.new(arr)
        end
      )
      publicKey = OpenSSL::ASN1::Sequence.new([n,sigma])

      version = OpenSSL::ASN1::PrintableString.new(PathgraphEncoding::VERSION)
      instance = OpenSSL::ASN1::Sequence.new([version,publicKey])

      instance.to_der
    end

    # Decode and build a instance of a pathgraph public key from a
    # DER-formatted bytes array.
    #
    # == Parameters:
    # der::
    #   A byte array representing the public key specified.
    #
    # == Returns:
    # key::
    #   The public key dencoded.
    #
    def self.from_der(der)
      # Decode DER bytes
      asn1 = OpenSSL::ASN1.decode(der)

      # Public key build from DER bytes
      {
        version: OpenSSL::ASN1.decode(asn1.value[0]).value,
        n: OpenSSL::ASN1.decode(asn1.value[1].value[0]).value.to_i,
        sigma: OpenSSL::ASN1.decode(asn1.value[1].value[1]).value.map { |e| e.map { |d| d.value.to_i } }
      }
    end

    # Check if is a valid public key.
    # 
    # === Implemented validations
    # 
    # - Hypercube degree (n) is valid if:
    #   - Is an integer, such that is greater than 4
    #     (`PathgraphEncoding::MIN_Q_N`).
    # - Set (sigma) of pairs of endpoint vertices is valid if:
    #   - For each pair of vertices in set, each vertex is:
    #     - A non negative integer, such that is less than 2 to n.
    #
    # == Parameters:
    # key::
    #   A Hash object representing the public key specified.
    #
    # == Returns:
    # Returns `true` if key satisfies implemented validations,
    # otherwise `false`.
    #
    def self.is_valid?(key)
      # Check valid hypercube degree (n)
      return false if !key[:n].is_a? Integer || key[:n]<MIN_Q_N

      # Check vertices valid in hypercube Q_n
      max_vertex = (1<<key[:n])-1
      key[:sigma].flatten.each do |vertex|
        return false if !vertex.is_a? Integer || vertex>max_vertex || vertex<0
      end

      true
    end

  end

end
